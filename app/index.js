import React from "react";

import Layout from "../src";

export default () => {
  return (
    <Layout
      title="Title is here!!!"
      description="This detailed description is provided here, offering both succinct insights and in-depth analyses as needed. It covers a wide range of perspectives about the subject matter, making it adaptable to contexts that require brief overviews or comprehensive explanations. Whether the content needs to be concise or expansive, this explanation serves as a flexible guideline to suit various informational needs, ensuring clarity and completeness at every level of discussion."
      // progress={true}
      // contentFlexGrow={1}
      controls={[
        { title: "Control 1", action: () => alert('Control 1') },
        { title: "Control 2", action: () => alert('Control 2') },
      ]}
      // controlsJustifyContent="flex-end"
    >
      <div>
        Some content or components here!
      </div>
    </Layout>
  )
}