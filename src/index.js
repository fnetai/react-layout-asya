import React from 'react';

import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import LinearProgress from '@mui/material/LinearProgress';
import { useTheme } from '@mui/material/styles';
import useLayout from "./useLayout";

import "./index.scss";

export default ({ children, ...props }) => {
  const containerRef = React.useRef();
  const theme = useTheme();

  // Custom hook for layout calculations
  const { ready, layout, level, area } = useLayout({ containerRef });

  // A form object to expose methods for controlling the component externally
  const form = props.form || {};

  // State values for external control via form export
  const [title, setTitle] = React.useState(props.title?.value || props.title);
  const [titleAlignSelf, setTitleAlignSelf] = React.useState(props.title?.alignSelf || props.titleAlignSelf);

  // Progress properties
  const [progress, setProgress] = React.useState(props.progress?.value || props.progress);
  const [progressAlignSelf, setProgressAlignSelf] = React.useState(props.progress?.alignSelf || props.progressAlignSelf);

  // Description properties
  const [description, setDescription] = React.useState(props.description?.value || props.description);
  const [descriptionAlignSelf, setDescriptionAlignSelf] = React.useState(props.description?.alignSelf || props.descriptionAlignSelf);

  // Content layout properties
  const [contentAlignSelf, setContentAlignSelf] = React.useState(
    props.layout?.content?.alignSelf || props.layout?.contentAlignSelf || props.contentAlignSelf || 'stretch'
  );

  const [contentFlexGrow, setContentFlexGrow] = React.useState(
    props.layout?.content?.flexGrow || props.layout?.contentFlexGrow || props.contentFlexGrow || undefined
  );

  const [contentOverflow, setContentOverflow] = React.useState(
    props.layout?.content?.overflow || props.layout?.contentOverflow || props.contentOverflow || 'visible'
  );

  // Controls properties
  const [controls, setControls] = React.useState(props.controls || []);
  const [controlsAlignSelf, setControlsAlignSelf] = React.useState(
    props.controls?.alignSelf || props.controlsAlignSelf
  );

  const [controlsJustifyContent, setControlsJustifyContent] = React.useState(props.controls?.justifyContent || props.controlsJustifyContent || 'flex-start');
  // const [controlsAlignItems, setControlsAlignItems] = React.useState(props.controls?.alignItems || props.controlsAlignItems || 'center');

  // Container properties
  const [containerJustifyContent, setContainerJustifyContent] = React.useState(
    props.layout?.container?.justifyContent || props.layout?.containerJustifyContent || 'center'
  );
  const [containerAlignItems, setContainerAlignItems] = React.useState(
    props.layout?.container?.alignItems || props.layout?.containerAlignItems || 'flex-start'
  );
  const [containerRowGap, setContainerRowGap] = React.useState(
    props.layout?.container?.rowGap || props.layout?.containerRowGap || 1
  );
  const [containerColor, setContainerColor] = React.useState(
    props.layout?.container?.color || props.layout?.containerColor || theme.palette.text.primary
  );
  const [containerBackgroundColor, setContainerBackgroundColor] = React.useState(
    props.layout?.container?.backgroundColor || props.layout?.containerBackgroundColor || "inherit"
  );
  const [containerOverflow, setContainerOverflow] = React.useState(
    props.layout?.container?.overflow || props.layout?.containerOverflow || 'visible'
  );

  // Function to calculate padding dynamically based on layout
  const calculatePadding = React.useCallback(
    (side) => {
      const spacingMultiplier = layout === 'horizontal' ? 2 : 1;
      const isHorizontalSide = side === 'left' || side === 'right';
      const isVerticalSide = side === 'top' || side === 'bottom';

      if (isHorizontalSide && layout === 'horizontal') {
        return theme.spacing(area * level * spacingMultiplier);
      }

      if (isVerticalSide && layout === 'vertical') {
        return theme.spacing(area * level);
      }

      return theme.spacing(area);
    },
    [layout, level, area, theme]
  );

  // Memoized style object to avoid recalculating styles on every render
  const containerStyle = React.useMemo(
    () => ({
      "--spacing-unit": theme.spacing(1),
      "--padding-left": calculatePadding('left'),
      "--padding-right": calculatePadding('right'),
      "--padding-top": calculatePadding('top'),
      "--padding-bottom": calculatePadding('bottom'),
      "--font-family": theme.typography.fontFamily,
      "--area": area,
      "--level": level,
      "--layout": layout,
      "--primary-color": theme.palette.primary.main,
      "--secondary-color": theme.palette.secondary.main,
      "--text-primary": theme.palette.text.primary,
      "--text-secondary": theme.palette.text.secondary,
      "--background-default": theme.palette.background.default,
      "--background-paper": theme.palette.background.paper,
      "--border-radius": `${theme.shape.borderRadius}px`,
      "--box-shadow": theme.shadows[1],
      "--row-gap": theme.spacing(containerRowGap),
    }),
    [theme, calculatePadding, area, level, layout, containerRowGap]
  );

  // Export methods via the form object
  React.useEffect(() => {
    if (form) {
      form.setTitle = setTitle;
      form.setDescription = setDescription;
      form.setProgress = setProgress;
      form.setControls = setControls;
      form.setContainerJustifyContent = setContainerJustifyContent;
      form.setContainerAlignItems = setContainerAlignItems;
      form.setContainerRowGap = setContainerRowGap;
      form.setContainerColor = setContainerColor;
      form.setContainerBackgroundColor = setContainerBackgroundColor;
      form.setContainerOverflow = setContainerOverflow;
      form.setContentAlignSelf = setContentAlignSelf;
      form.setContentFlexGrow = setContentFlexGrow;
      form.setContentOverflow = setContentOverflow;
      form.setTitleAlignSelf = setTitleAlignSelf;
      form.setDescriptionAlignSelf = setDescriptionAlignSelf;
      form.setProgressAlignSelf = setProgressAlignSelf;
      form.setControlsAlignSelf = setControlsAlignSelf;
    }
  }, [
    form,
    setTitle,
    setDescription,
    setProgress,
    setControls,
    setContainerJustifyContent,
    setContainerAlignItems,
    setContainerRowGap,
    setContainerColor,
    setContainerBackgroundColor,
    setContainerOverflow,
    setContentAlignSelf,
    setContentFlexGrow,
    setContentOverflow,
    setTitleAlignSelf,
    setDescriptionAlignSelf,
    setProgressAlignSelf,
    setControlsAlignSelf,
  ]);

  return (
    ready && (
      <div
        ref={containerRef}
        className="asya-container"
        style={{
          ...containerStyle,
          overflow: containerOverflow,
          backgroundColor: containerBackgroundColor,
          color: containerColor,
          alignItems: containerAlignItems,
          justifyContent: containerJustifyContent,
        }}
      >
        {title && (
          <div className="asya-title-container" style={{ alignSelf: titleAlignSelf }}>
            <Typography sx={{ fontSize: "2.5rem" }} >{title}</Typography>
          </div>
        )}

        {progress && (
          <div className="asya-progress-container" style={{ alignSelf: progressAlignSelf }}>
            <LinearProgress />
          </div>
        )}

        {description && (
          <div className="asya-description-container" style={{ alignSelf: descriptionAlignSelf }}>
            <Typography sx={{ fontSize: "1.5rem" }}>{description}</Typography>
          </div>
        )}

        {children && (
          <div
            className="asya-children-container"
            style={{
              alignSelf: contentAlignSelf,
              flexGrow: contentFlexGrow,
              overflow: contentOverflow,
            }}
          >
            {children}
          </div>
        )}

        {controls.length > 0 && (
          <div className="asya-controls-container" style={{ alignSelf: controlsAlignSelf }}>
            <div style={{ display: "flex", justifyContent: controlsJustifyContent, gap: theme.spacing(2) }}>
              {controls.map((item, index) => (
                <Button
                  key={index}
                  autoFocus={item.focus === true || index === 0}
                  focusRipple={true}
                  size={item.size || "small"}
                  variant={item.variant || "contained"}
                  color={item.color || "primary"}
                  onClick={item.action || props.actionClick}
                >
                  {item.title || "OK"}
                </Button>
              ))}
            </div>
          </div>
        )}
      </div>
    )
  );
};