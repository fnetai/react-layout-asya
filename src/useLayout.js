import { useEffect, useState } from 'react';

export default ({ containerRef }) => {
    // Declare a state variable to store the window width, height, resolution, aspect ratio, platform, and device category
    const [dimensions, setDimensions] = useState({
        width: 0,
        height: 0,
        layout: '',
        level: 0,
        area: 0,
        fontSize: 0,
        ready: false
    });
    // Declare a function to handle window resize events
    function handleResize() {
        // Get the current window width and
        const width = (containerRef.current || document.body).offsetWidth;
        const height = (containerRef.current || document.body).offsetHeight;

        const step = Math.floor(Math.max(width, height) / 10);
        const widthCell = Math.floor((width / step) + 0.49);
        const heightCell = Math.floor((height / step) + 0.49);

        const horizontalCellRatio = widthCell / heightCell;
        const verticalCellRatio = heightCell / widthCell;

        let layout;
        let level;

        if (horizontalCellRatio > 1.2) {
            layout = 'horizontal';
            level = Number.parseFloat(horizontalCellRatio.toFixed(1));
        }

        else if (verticalCellRatio > 1.2) {
            layout = 'vertical';
            level = Number.parseFloat(verticalCellRatio.toFixed(1));
        }
        else {
            layout = 'square';
            level = Number.parseFloat(Math.max(horizontalCellRatio, verticalCellRatio).toFixed(1));
        }

        if (level > 5) level = 5;

        // iPhone SE as base
        const widthBase = 375;
        const heightBase = 667;

        const widthCols = (width / widthBase);
        const heightRows = (height / heightBase);

        const area = Number.parseFloat((widthCols * heightRows).toFixed(1));

        const fontSize = Number.parseInt((Math.min(width, height) / 14).toFixed(1));

        // Update the dimensions state variable with the current window width, height, resolution, aspect ratio, platform, and device category
        setDimensions({
            width,
            height,
            layout,
            level,
            area,
            fontSize,
            ready: true
        });
    }

    // Add a window resize event listener when the component mounts
    useEffect(() => {
        window.addEventListener('resize', handleResize);

        handleResize();

        // Remove the event listener when the component unmounts
        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    return dimensions;
}